# Basic REST API Application 
Приложение создано для ознакомления со структурой SpringBoot Rest API приложения

# Создано по инструкции 
[Building REST services with Spring](https://spring.io/guides/tutorials/rest/)

### Getting Started
Импортировать проект необходимочерез build.gradle файл

### Автор
* Проказа Андрей [GitLab](https://gitlab.com/AngryAn) [Tlgrm](https://t.me/angryan)

